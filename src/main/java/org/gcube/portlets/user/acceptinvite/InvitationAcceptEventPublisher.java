package org.gcube.portlets.user.acceptinvite;

import org.gcube.portal.event.publisher.lr62.AbstractLR62EventPublisher;

public class InvitationAcceptEventPublisher extends AbstractLR62EventPublisher {
	public InvitationAcceptEventPublisher() {
        super();
    }
}
