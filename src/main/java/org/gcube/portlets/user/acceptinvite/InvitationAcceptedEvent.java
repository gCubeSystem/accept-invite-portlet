package org.gcube.portlets.user.acceptinvite;

import java.util.UUID;

import org.gcube.portal.event.publisher.lr62.PortalEvent;

import com.liferay.portal.security.auth.CompanyThreadLocal;
import com.liferay.portal.security.auth.DefaultScreenNameGenerator;
import com.liferay.portal.service.UserLocalServiceUtil;

public class InvitationAcceptedEvent extends PortalEvent {

    private static final long serialVersionUID = 1499288552188273747L;

    public static final String NAME = "invitation-accepted";
    public static final String FIRST_NAME_ENTRY = "first-name";
    public static final String LAST_NAME_ENTRY = "last-name";
    public static final String EMAIL_ENTRY = "email";
    public static final String PASSWORD_ENTRY = "password";

    private static DefaultScreenNameGenerator defaultScreenNameGenerator = new DefaultScreenNameGenerator();

    public InvitationAcceptedEvent(String firstname, String lastname, String email, String password) {
        super(NAME);
        setUser(computeUsername(email));
        setFirstname(firstname);
        setLastname(lastname);
        setEmail(email);
        setPassword(password);
    }

    protected String computeUsername(String email) {
        try {
            Long companyId = PortletViewController.getCompany().getCompanyId();
            Long defaultUserId = UserLocalServiceUtil.getDefaultUserId(companyId);
            String screename = defaultScreenNameGenerator.generate(companyId, defaultUserId, email);
            //here we add some random char to the screenname created
            StringBuilder sb = new StringBuilder(screename)
            		.append(UUID.randomUUID().toString().substring(0, 5));
            String computedUsername =  sb.toString();
            return computedUsername;
        } catch (Exception e) {
            log.error("Cannot generate username via screen name generator", e);
            return "";
        }
    }

    public void setFirstname(String firstname) {
        set(FIRST_NAME_ENTRY, firstname);
    }

    public String getFirstname() {
        return (String) get(FIRST_NAME_ENTRY);
    }

    public void setLastname(String lastname) {
        set(LAST_NAME_ENTRY, lastname);
    }

    public String getLastname() {
        return (String) get(LAST_NAME_ENTRY);
    }

    public void setEmail(String email) {
        set(EMAIL_ENTRY, email);
    }

    public String getEmail() {
        return (String) get(EMAIL_ENTRY);
    }

    public void setPassword(String password) {
        set(PASSWORD_ENTRY, password);
    }

    public String getPassword() {
        return (String) get(PASSWORD_ENTRY);
    }

}
