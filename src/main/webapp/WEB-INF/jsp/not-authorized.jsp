<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<portlet:defineObjects />

<div class="portlet-content">
	<h3 class="alert alert-error">Forbidden</h3>
	You do not have permission to access this page. <br>
	<br>
	 If you need support for manage your invitation please go to <a
		href="http://www.d4science.org/contact-us" target="_blank">http://www.d4science.org/contact-us</a> to ask for D4Science Help Desk support. <br>
	<br />If you were trying to access a VRE make sure you are member,
	you can ask to be member in the <a 
		href="/explore">Explore page</a> of this gateway. Please click <a
		 href="/explore">here</a> to do so. <br>
	<br />
	<div class="separator">
		<!-- -->
	</div>



</div>