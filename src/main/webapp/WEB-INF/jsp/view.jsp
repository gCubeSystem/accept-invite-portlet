<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="javax.portlet.*"%>
<%@ page import="org.gcube.common.portal.*"%>
<%@ page import=" org.gcube.portal.invites.InvitesManager"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ page import="java.net.URLEncoder"%>

<portlet:defineObjects />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<portlet:resourceURL id="createAccount" var="createAccount"></portlet:resourceURL>

<script type="text/javascript">
	$(document).ready(function() {
		doCallback = function() {
			$('#formCreateAccountWrapper').css("display", "none");// to clear the previous option
			$('#loadingAjaxCallback').css("display", "block");
			$.ajax({
				url : "${createAccount}",
				type : 'POST',
				datatype : 'json',
				data : {
					email : $("#email").val(),
					firstname : $("#firstname").val(),
					lastname : $("#lastname").val()
				},
				success : function(data) {
					$('#loadingAjaxCallback').css("display", "none");
					var result = data.toString().trim();
					if (result == "OK") {
						$('#successDiv').css("display", "block");
					} else {
						$('#errorDiv').css("display", "block");
						$('#errorLabel').text(data);
					}
				}
			});
		}
	});
</script>

<%
	String academicRedirectURL = request.getAttribute("landingPage") + "/explore?" + InvitesManager.SITEID_ATTR
			+ "=" + request.getAttribute("groupId");
    pageContext.setAttribute("academicRedirectURL", academicRedirectURL);
%>

<div id="errorDiv" style="display: none;" class="alert alert-error">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<h1>Oh snap! You got an error</h1>
	<label id="errorLabel"></label> <br> <br /> If you believe this
	requires support please go to <a
		href="http://www.d4science.org/contact-us" target="_blank">http://www.d4science.org/contact-us</a>
	to ask for D4Science Help Desk support. <br> <br />
</div>

<div id="successDiv" style="display: none;" class="alert alert-success">
	<h1>Well done!</h1>
	Your account is being created! You will be now asked to
	sign in: use your email and the temporary password received in the invite email, then choose your password. Please, click on the button
	below to sign in.
	<%
		String acceptInviteURL = request.getAttribute("landingPage") + "/explore?" + InvitesManager.SITEID_ATTR
				+ "=" + request.getAttribute("groupId");
	%>
	<p style="margin-top: 20px;">
		<button class="btn btn-primary btn-lg btn-fill"
			onclick="window.location.href='<%=acceptInviteURL%>'" type="button">
			Continue accept invite on
			<c:out escapeXml="true" value="${vreName}" />
		</button>
	</p>
</div>
<div id="formCreateAccountWrapper">
	<p>
		<span class="lead"> <b>Hello <c:out escapeXml="true"
					value="${invitedUser.firstName}" /> !
		</b>You received an invitation from <c:out escapeXml="true"
				value="${inviteInstance.senderFullName}" /> to join <a
			href="/web<c:out escapeXml="true" value="${vreFriendlyURL}" />"
			target="_blank"><c:out escapeXml="true" value="${vreName}" /></a>.
		</span>
	</p>
	<p class="lead">
		<img id="loadingAjaxCallback" style="display: none;"
			src="<%=renderRequest.getContextPath()%>/images/loader.gif" />
		<c:choose>
			<c:when test="${empty invitedUser}">
				To accept the invite, you can either create a new account entering your data below or 
				<a href="${academicRedirectURL}">sign up with your
					academic/social account</a>.
        <br />

				<div class="container-fluid">
					<div class="row-fluid">
						<div class="span4">
							<fieldset>
							<label id="labelFirstName">First
									Name (Required)</label> <input type="text" id="firstname" /> <label
									id="labelLastName">Last Name (Required)</label> <input
									type="text" id="lastname" />
							</fieldset>
						</div>
						<div class="span4">
							<fieldset>
<!-- 								<label id="labelPwd1">Password</label> <input type="password" -->
<!-- 									id="password" /> <label id="labelPwd2">Confirm -->
<!-- 									Password</label> <input type="password" id="repassword" /> <label -->
<!-- 									style="display: none" id="labelPasswordDontMatch">Passwords -->
<!-- 									don't match</label> <label style="display: none" -->
<!-- 									id="labelPasswordTooShort">Password must be at least 8 -->
<!-- 									chars length</label> -->
								<label>Email (Not editable)</label> <input type="text"
									name="email" value="${inviteInstance.invitedEmail}" readonly
									id="email" style="color: #999;" /> 
								<div style="margin-top: 20px;">
									<button class="btn btn-primary btn-lg btn-fill" type="button"
										id="createAccountButton">Continue</button>
								</div>
							</fieldset>
						</div>
					</div>
				</div>
				<p><br />
					<c:if test="${empty invitedUser}">
						<span><b>Please note</b>: this invite is only valid for
							your email <a
							href="mailto:<c:out escapeXml="true" value="${inviteInstance.invitedEmail}" />">
								<c:out escapeXml="true" value="${inviteInstance.invitedEmail}" />
						</a>. If you own a <a
							href="https://services.d4science.org/thematic-gateways"
							target="_blank">D4Science account</a> or are willing to use a different email, please ask to get the invite by a different email. </span>
				</p>
				</c:if>
			</c:when>
			<c:otherwise>
    This invite is valid for your email <a
					href="mailto:<c:out escapeXml="true" value="${inviteInstance.invitedEmail}" />">
					<c:out escapeXml="true" value="${inviteInstance.invitedEmail}" />
				</a> only, please click the button below and login with your credentials. 
 
 <%
					String exploreURL = request.getAttribute("landingPage") + "/explore?" + InvitesManager.SITEID_ATTR
									+ "=" + request.getAttribute("groupId");
				%>
				<p class="lead">
					<button class="btn btn-large btn-primary btn-fill"
						onclick="window.location.href='<%=exploreURL%>'" type="button">
						Accept invite on
						<c:out escapeXml="true" value="${vreName}" />
					</button>
				</p>
			</c:otherwise>
		</c:choose>
	</p>
</div>