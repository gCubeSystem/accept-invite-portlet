$(document).ready(function(){	
	$( "#createAccountButton" ).click(function() {
		var firstnameBox =  $.trim( $('#firstname').val() )
		if (firstnameBox == "") {
			$('#labelFirstName').css("color","red");
			$('#labelFirstName').text("First Name (This field is required)");
		} else {
			$('#labelFirstName').css("color","#555");
			$('#labelFirstName').text("First Name (Required)");
		}
		var lastnameBox =  $.trim( $('#lastname').val() )
		if (lastnameBox == "") {
			$('#labelLastName').css("color","red");
			$('#labelLastName').text("Last Name (This field is required)");
		} else {
			$('#labelLastName').css("color","#555");
			$('#labelLastName').text("Last Name (Required)");
		}

		if (lastnameBox != "" && firstnameBox != "") {
			doCallback();
		}
	
	});
});