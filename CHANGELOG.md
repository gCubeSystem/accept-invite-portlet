
# Changelog for Accept Invite portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.1.0] - 2024-04-22
- Fix for Incident #27306

## [v2.0.1] - 2022-06-16

 - Release for HL portal removal

## [v2.0.0] - 2020-07-21

 - Feature #19463, gestione Inviti include Keycloak per quanto riguarda la creazione dello user account

 - Ported to git

## [v1.2.0] - 2019-08-10

Modified text to make evident you can create an account via SSO

## [v1.1.0] - 2018-02-22

The created account now automatic set email as verified

## [v1.0.0] - 2017-12-06

First release 
